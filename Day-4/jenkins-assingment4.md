# Learning by Doing | Jenkins Setup

## Introduction
In this section we will work on Master slave setup in Jenkins and how to configure it acc to requirement.

## References
*

## Assignments
### Must Do

* Assignment1: 
	1. Create a jenkins slave and add it to jenkins master.
	2. Restrict jobs for jenkins slave which you have created.
	3. Create a job that can run on either on master or slave.

 
![image](images/conn_param.png)
![image](images/statusofnode.png)
![image](images/eithermors.png)
![image](images/jobslave.png)

* Assignmnet2:
	1. Use jenkins rest api to Trigger a jenkins job build
		1. Get the list of all jobs 
		2. Get the last build status
		3. Get the console o/p of a job build. 
		4. Trigger a jenkins job build
```
1. curl -u admin:112e3a897f5674259ed508d588b6e86a99 http://15.206.100.85:8080/api/json?pretty=true
2. curl -X GET http://15.206.100.85:8080/api/json --user admin:11ed2b9dd6e7d06c7f82642d1c0589fd82 | jq -r '.result'
3. curl -u  admin:11ed2b9dd6e7d06c7f82642d1c0589fd82  "http://15.206.100.85:8080/job/heloo/lastBuild/consoleText"
4. curl -X GET http://15.206.100.85:8080/job/heloo/api/json --user admin:11ed2b9dd6e7d06c7f82642d1c0589fd82 | jq  '.color' 
```
