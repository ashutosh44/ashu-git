# Learning by Doing | Jenkins Setup

## Introduction
In this section we will learn CI concept

## Assignments
### Must Do
* Create a pipeline job for CI which will include below stages.
  1. Checkout code (Clone spring3hibernate)
  2. Code Stability
  3. Code Quality
  4. Code Coverage
  5. Email Notification (Notification must contains job console URL and the username who started the job)
  6. Slack Notification (Notification must contains job console URL and the username who started the job)
```

```
* Publish Code coverage and checkstyle report in above pipeline job
* If issues in above health reports is greater than 2, then job should get failed.   
* Create a Job DSL for above pipeline job and using Job DSL, create same pipeline job with different name. 
```
pipeline {
  agent any
  stages
  {
      stage('Clone git code')
      {
          steps{
          sh 'rm -rf spring3hibernate;  git clone https://gitlab.com/arunangshu_ops/spring3hibernate.git'
          }
      }
      stage('Email Notification')
      {
          steps{
          emailext body: 'Jenkins Pipeline', recipientProviders: [developers()], subject: 'Batch 6 Ninja Assignment day 5', to: 'ashu10996@gmail.com'
              }
          }
      stage('Slack Notification')
      {
          steps{
          slackSend channel: 'aruntest', color: 'green', iconEmoji: '', message: 'Deployment processing by pipeline-ashu', tokenCredentialId: 'Slack_Notification', username: ''
              }
          }
      stage('Code Stability')
      {
          steps{
          sh 'cd spring3hibernate; mvn compile'
          }
      }
      stage('Code quality')
     {
        steps{
        sh 'cd spring3hibernate; mvn checkstyle:checkstyle'
      //  checkstyle()
          }
    }
     stage('Code Coverage')
    {
         steps{
             sh 'cd spring3hibernate; mvn cobertura:cobertura'
        //     cobertura()
         }
     
 stage('Publish coverage')
        {
          steps{
                 cobertura autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'spring3hibernate/target/site/cobertura/coverage.xml', conditionalCoverageTargets: '70, 0, 0', enableNewApi: true, failUnhealthy: false, failUnstable: false, lineCoverageTargets: '80, 0, 0', maxNumberOfBuilds: 0, methodCoverageTargets: '80, 0, 0', onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false
                // checkstyle()
                // cobertura()
               }
        }
      stage('Generate war file')
      {
          steps{
              sh 'cd spring3hibernate; mvn install'
          }
      }
      stage('Slack Final Notification')
      {
          steps{
              slackSend channel: 'aruntest', color: 'green', iconEmoji: '', message: 'Success', tokenCredentialId: 'Slack_Notification', username: ''
          }
      }
       }
    }
```
### Good to Do
* Write scripted pipeline instead of declarative pipeline for above task.
* Instead of using "sh" parameter in pipeline script for maven commands, use maven plugin
* Create functions for slack and email notification in pipeline script.
