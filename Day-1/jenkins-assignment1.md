## Assignments
### Must Do
* Create a freestyle job to print "Hello world".

```
echo "Hello World"

```

* Create a freestyle job to which take absolute path of a directory then
    * List all files and directories inside that.
    * Print a message "drectory not exist" if directory doesn't exist on file system
    * Print "Inappropriate permissions" if you don't have permissions to list files.

```

if ! [ -d "$dir" ]
then 
echo "directory not exist"
else
    if  [ -x "$dir" ]
        then
        echo "inappropriate permission"
    fi
fi

```

* Update the previously created freestyle job to only retain last 10 build history but not beyond 2 days.

![image](images/10logs2day.png)

* Clone the code available in the same repository.
    * Using git protocol
    * Using ssh protocol

![image](images/gitprotocol.png)

![image](images/sshprotocol.png)
 
* Update above jenkins job so that it should be able to identify if there is a code commit in last 5 minutes it should get triggered.

![image](images/5-mins.png)

* Enable colored console output Using Ansicolor plugin

![image](images/coloroutput.png)


### Good to Do
* Clone the code available in the same repository only if there is changes in *java* folder and only this folder should be checked out.

![image](images/depth.png)

* Integrate above Jenkins job to integrate with GitLab so that any commit in last 5 minutes will trigger this Jenkins job

![image](images/completedepth.png)

* Summarize all important Additional checkout behaviour



## Summary
In this section we have gone through various options of freestyle Jenkins job.


# Learning by Doing | Jenkins Setup

## Introduction
In this section we will work on Security aspect of Jenkins along with some other Jenkins jobs

## References
*

## Assignments
### Must Do
* Create a Jenkins job(InstallPackage) that will take a package name as input and install it in local system.

![image](images/installpackage1.png)

* Modify InstallPackage jenkins job to take remote system IP as input to install the package.

![image](images/remotepackageinstall.png)

* Modify InstallPackage jenkins job so that it should be able to support different types of OS such as:
  * Ubuntu 14/16
  * CentOS 6/7

* Modify InstallPackage jenkins job to take additional parameters for remote system to be managed:
  * ssh username
  * PrivateKey file

![image](images/oscheck.png)

* Modify InstallPackage jenkins job where a drop down will be provided of remote systems to be managed.

![image](images/oscheck.png)

* Modify InstallPackage jenkins job where checkboxes are provided for multiple remote systems.

![image](images/checkbox2.png)


* Modify InstallPackage jenkins job which will take a meta file as an input in below format

```host, ip, user, private_key_file, package```

the above said file can have 1 or more entries

* Create below users in your system
  * dev
  * qa
  * devops
* qa user should be allowed only to execute the InstallPackage jenkins job
* dev user should be allowed to execute and view the content of InstallPackage jenkins job
* devops user should be admin of Jenkins server

![image](images/createuser.png)
![image](images/roles1.png)

### Good to Do
* Implement InstallPackage using Jenkinsfile 
```

```
* Use gmail for Authentication

![image](images/mailconfig.png)


## Summary
In this section you learned about Authentication & Authorization
